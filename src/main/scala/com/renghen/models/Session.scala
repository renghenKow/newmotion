package com.renghen.models

import java.sql.Timestamp
import slick.jdbc.MySQLProfile.api._
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsString, JsonWriter}
import com.renghen.models.utils.DateUtilities._

case class Session(customerId: String, startTime: Timestamp
                   , endTime: Timestamp, volume: Double
                   , id: Option[Long] = None)


object SessionJsonProtocol extends DefaultJsonProtocol {

  implicit object SessionJsonFormat extends JsonWriter[Session] {
    def write(session: Session) = JsObject(
      "customerId" -> JsString(session.customerId),
      "startTime" -> JsString(dateFormatter.format(session.startTime)),
      "endTime" -> JsString(dateFormatter.format(session.endTime)),
      "volume" -> JsNumber(session.volume)
    )
  }

}

trait SessionTable {

  class Sessions(tag: Tag) extends Table[Session](tag, "session") {
    def id: Rep[Option[Long]] = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc) // primary key,just a dummy one

    def customerId: Rep[String] = column[String]("customerId")

    def startTime: Rep[Timestamp] = column[Timestamp]("startTime")

    def endTime: Rep[Timestamp] = column[Timestamp]("endTime")

    def volume: Rep[Double] = column[Double]("volume")

    def * = (customerId, startTime, endTime, volume, id) <> ((Session.apply _).tupled, Session.unapply)
  }

  protected val sessions = TableQuery[Sessions]

}
