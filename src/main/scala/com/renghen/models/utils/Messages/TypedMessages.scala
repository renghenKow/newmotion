package com.renghen.models.utils.Messages

sealed trait ValidationMessage {
  def message: String
}

case object UnknownOutputType extends ValidationMessage{
  override def message: String = "unknown output type"
}

case object SuccessFullSessionInsertion extends ValidationMessage {
  override def message: String = "Session successfully inserted"
}

case object InvalidDateRange extends ValidationMessage{
  override def message: String = "InvalidDateRange - 'startTime' is greater than or equal to 'endTime'"
}

case object EmptyValue extends ValidationMessage{
  override def message: String = "Empty value"
}

case object SuccessFullTariffInsertion extends ValidationMessage {
  override def message: String = "Tariff successfully inserted"
}

case object TooEarly extends ValidationMessage{
  override def message: String = "Date was too early"
}

case object UnknownError extends ValidationMessage {
  override def message: String = "Unknown error"
}

case object NoPricesPresent extends ValidationMessage{
  override def message: String = "No price present"
}

case object NoJsonFormat extends ValidationMessage {
  override def message: String = "Not in Json Format"
}

case class AbsentField(field: String) extends ValidationMessage {
  override def message: String = s"$field is not set."
}

case class InValidPriceFormat(field: String) extends ValidationMessage {
  override def message: String = s"$field has invalid integer format."
}

case class InValidPrice(field: String) extends ValidationMessage {
  override def message: String = s"$field is not a valid number."
}

case class InValidCurrency(field: String) extends ValidationMessage {
  override def message: String = s"$field is an invalid Currency."
}

case class InvalidDate(dateField: String) extends ValidationMessage {
  override def message: String = s"$dateField has invalid date format."
}

