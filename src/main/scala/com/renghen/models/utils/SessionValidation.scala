package com.renghen.models.utils

import java.sql.Timestamp
import java.util.Date

import com.renghen.models.Session
import com.renghen.models.utils.Messages.{EmptyValue, InvalidDateRange, ValidationMessage}
import com.renghen.repository.TariffRepository
import com.renghen.utils.DateUtil

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

//cats lib
import cats.syntax.either._

object SessionValidation {

  import ValidateFunctions._

  def validateSessionFormat(str: String): Either[List[ValidationMessage], Session] = {
    val isJs = isJson(str)
    val buffer = mutable.Buffer[ValidationMessage]()

    if (isJs.isLeft) {
      List(isJs.left.get).asLeft[Session]
    }
    else {
      val js = isJs.right.get

      val iscustomerIdCorrect = isFieldPresent("customerId", js)
      val customerId = iscustomerIdCorrect match {
        case Left(err) =>
          buffer += err
          ""
        case Right(cus) => val valueJs = js.asJsObject.getFields(cus).head.toString()
          if (valueJs.isEmpty) {
            buffer += EmptyValue
            ""
          } else trimQuotes(valueJs)
      }

      val isValidStartTime = isFieldValidDate("startTime", js)
      val startTimeStr = isValidStartTime match {
        case Left(err) =>
          buffer += err
          ""
        case Right(dateStr) =>
          dateStr
      }

      val isValidEndTime = isFieldValidDate("endTime", js)
      val endTimeStr = isValidEndTime match {
        case Left(err) =>
          buffer += err
          ""
        case Right(dateStr) =>
          dateStr
      }

      val isValidVolume = isFieldValidVolume("volume", js)
      val volume = isValidVolume match {
        case Left(err) =>
          buffer += err
          0.0d
        case Right(volumeValue) => volumeValue
      }

      if (buffer.nonEmpty) {
        buffer.toList.asLeft[Session]
      } else {
        val startTime = new Timestamp(DateUtil.parseRFC3339Date(startTimeStr).getTime)
        val endTime = new Timestamp(DateUtil.parseRFC3339Date(endTimeStr).getTime)

        if (endTime.compareTo(startTime) > 0)
          Session(customerId, startTime, endTime, volume).asRight[List[ValidationMessage]]
        else
          List(InvalidDateRange).asLeft[Session]
      }
    } //end of json validation

  } //end of def

}

//end of object
