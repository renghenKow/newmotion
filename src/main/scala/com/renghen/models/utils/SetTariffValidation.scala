package com.renghen.models.utils

import java.sql.Timestamp
import java.util.Date

import com.renghen.models.Tariff
import com.renghen.models.utils.Messages.{NoPricesPresent, TooEarly, ValidationMessage}
import com.renghen.repository.TariffRepository
import com.renghen.utils.DateUtil

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

//cats lib
import cats.syntax.either._

object SetTariffValidation {

  import ValidateFunctions._

  def validateSetTariffFormat(str: String): Either[List[ValidationMessage], Tariff] = {
    val isJs = isJson(str)
    val buffer = mutable.Buffer[ValidationMessage]()

    if (isJs.isLeft) {
      List(isJs.left.get).asLeft[Tariff]
    }
    else {
      val js = isJs.right.get
      val isCurrencyCorrect = isValidCurrency("currency", js)

      val currency = isCurrencyCorrect match {
        case Left(err) =>
          buffer += err
          ""
        case Right(cur) => cur
      }

      val isValidDate = isFieldValidDate("activeStarting", js)
      val activeStartingStr = isValidDate match {
        case Left(err) =>
          buffer += err
          ""
        case Right(dateStr) =>
          val date = DateUtil.parseRFC3339Date(dateStr)
          val timeStamp = new Timestamp(date.getTime)
          val now = new Timestamp(new Date().getTime)

          if (timeStamp.compareTo(now) > 0)
            dateStr
          else {
            buffer += TooEarly
            ""
          }
      }

      //check for fees present
      val feeNames = List("startFee", "hourlyFee", "feePerKwh")
      val feesPresent = feeNames.map { fee => isFieldPresent(fee, js) }
        .filter { fee => fee.isRight }.map { fee => fee.right.get }

      //check for valid fees
      val mapFees = if (feesPresent.isEmpty) {
        buffer += NoPricesPresent
        Map[String, Either[ValidationMessage, Double]]()
      }
      else {
        feesPresent.map(feeName => (feeName, isFieldValidPrice(feeName, js))).toMap
      }

      mapFees.keys.foreach { key =>
        val value = mapFees(key)
        value match {
          case Left(error) => buffer += error
          case _ =>
        }
      }

      if (buffer.nonEmpty) {
        buffer.toList.asLeft[Tariff]
      } else {
        val mapValidFees: Map[String, Double] = mapFees.map { case (key, value) => (key, value.right.get) }
        val activeStarting = DateUtil.parseRFC3339Date(activeStartingStr)
        val sqlDate = new Timestamp(activeStarting.getTime)
        Tariff(currency, sqlDate, mapValidFees.get("startFee"),
          mapValidFees.get("hourlyFee"), mapValidFees.get("feePerKwh")).asRight[List[ValidationMessage]]
      }
    } //end of json validation
  } //end of def

  def isTariffLatest(tariff: Tariff, tariffRepository: TariffRepository)(implicit ec: ExecutionContext): Future[Boolean] = {
    tariffRepository.findLatest().map { lastPossibleTariff =>
      lastPossibleTariff match {
        case None => true
        case Some(lastTariff) =>
          tariff.activeStarting.compareTo(lastTariff.activeStarting) > 0
      }
    }
  }//end of def

}//end of object
