package com.renghen.models.utils

//java lib
import java.text.ParseException

import com.renghen.models.utils.Messages._

//custom lib
import com.renghen.utils.DateUtil

//spray lib
import spray.json.{JsValue, JsonParser}
import spray.json.JsonParser.ParsingException

//cats lib
import cats.syntax.either._

object ValidateFunctions {

  def trimQuotes(str : String): String = {
    str.stripPrefix("\"").stripSuffix("\"")
  }

  def isJson(str: String): Either[ValidationMessage, JsValue] = {
    try {
      val parsedJson = JsonParser(str)
      parsedJson.asRight[ValidationMessage]
    } catch {
      case _: ParsingException => NoJsonFormat.asLeft[JsValue]
      case _: Throwable => Messages.UnknownError.asLeft[JsValue]
    }
  } //end of def isJson

  def isFieldPresent(field: String, jsonValue: JsValue): Either[AbsentField, String] = {
    val fields = jsonValue.asJsObject.fields
    Either.fromOption(fields.get(field),AbsentField(field)).map(_ => field)
  }

  def isValidCurrency(field: String, jsonValue: JsValue): Either[InValidCurrency, String] = {
    import LoadCurrencyCode._
    val fieldValue = trimQuotes(jsonValue.asJsObject.getFields(field)
      .head.toString().toUpperCase)
    Either.fromOption(currencyCodes.find(code => code == fieldValue), InValidCurrency(field))
  }

  def isFieldValidPriceFormat(field: String, jsonValue: JsValue): Either[InValidPriceFormat, String] = {
    val re = """^\d+(\.\d{1,2})?$""".r
    val fieldValue = jsonValue.asJsObject.getFields(field)
    Either.fromOption(re.findFirstIn(fieldValue.head.toString()), InValidPriceFormat(field))
  }

  def isFieldValidPrice(field: String, jsonValue: JsValue): Either[ValidationMessage, Double] = {
    val fieldValue = jsonValue.asJsObject.getFields(field).head.toString()
    try {
      fieldValue.toDouble.asRight[ValidationMessage]
    } catch {
      case _: NumberFormatException => InValidPrice(field).asLeft[Double]
      case _: Throwable => Messages.UnknownError.asLeft[Double]
    }
  }

  def isFieldValidVolume(field: String, jsonValue: JsValue): Either[ValidationMessage, Double] = {
    val fieldValue = jsonValue.asJsObject.fields.get(field)
    fieldValue match {
      case None => AbsentField(field).asLeft[Double]
      case Some(_) =>
        val value = jsonValue.asJsObject.getFields(field).head.toString()
        try {
          value.toDouble.asRight[ValidationMessage]
        } catch {
          case _: NumberFormatException => InValidPrice(field).asLeft[Double]
          case _: Throwable => Messages.UnknownError.asLeft[Double]
        }
    }
  }

  def isFieldValidDate(field:String,jsonValue : JsValue): Either[ValidationMessage, String] ={
    val fieldValue = trimQuotes(jsonValue.asJsObject.getFields(field).head.toString())
    try {
      DateUtil.parseRFC3339Date(fieldValue)
      fieldValue.asRight[ValidationMessage]
    } catch {
      case _: ParseException => InValidPrice(field).asLeft[String]
      case _: Throwable => Messages.UnknownError.asLeft[String]
    }
  }

}
