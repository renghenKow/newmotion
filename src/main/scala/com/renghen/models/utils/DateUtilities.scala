package com.renghen.models.utils

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Locale, TimeZone}

object DateUtilities {

  val dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault) //spec for RFC3339 with a 'Z'
  dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))

  def diffTimeStamp(time1 : Timestamp, time2 : Timestamp) = {
    time2.getTime - time1.getTime
  }

  def millisToHours(millis : Long) : Double = {
    millis / (1000.0d * 3600)
  }

}
