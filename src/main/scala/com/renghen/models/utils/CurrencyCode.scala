package com.renghen.models.utils
import purecsv.unsafe._

object LoadCurrencyCode {
  private case class EntityCurrencyCode(entity: String, alphabeticCode: String)

  private val listEntityCurrencyCode = CSVReader[EntityCurrencyCode]
    .readCSVFromFileName("./resources/codes-all_csv.csv",skipHeader = true)

  val currencyCodes =
    listEntityCurrencyCode.filter(ecc => !ecc.alphabeticCode.isEmpty)
      .map(ecc => ecc.alphabeticCode)
      .distinct
}
