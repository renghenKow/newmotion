package com.renghen.models

import java.sql.Timestamp

import com.renghen.models.utils.DateUtilities._
import com.renghen.models.utils.Messages.{SuccessFullSessionInsertion, UnknownOutputType}
import com.renghen.models.utils.SessionValidation
import com.renghen.repository.SessionRepository
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsString, JsonWriter}

import scala.collection.mutable
import scala.concurrent.{ExecutionContext, Future}

case class SessionNFees(customerId: String, startTime: Timestamp
                        , endTime: Timestamp, volume: Double
                        , id: Option[Long] = None, fees: Seq[Tariff]
                        , startFeeSubTotal: Double = 0.0d
                        , hourlyFeeSubTotal: Double = 0.0d
                        , feePerKWhSubTotal: Double = 0.0d
                        , total: Double = 0.0d)

object SessionNFeesJsonProtocol extends DefaultJsonProtocol {

  implicit object SessionNFeesJsonFormat extends JsonWriter[SessionNFees] {
    //import TariffJsonProtocol._
    def write(sessionNFees: SessionNFees) = JsObject(
      "customerId" -> JsString(sessionNFees.customerId),
      "startTime" -> JsString(dateFormatter.format(sessionNFees.startTime)),
      "endTime" -> JsString(dateFormatter.format(sessionNFees.endTime)),
      "volume" -> JsNumber(sessionNFees.volume),
      //"fees" -> JsObject(sessionNFees.fees.map{tariff => tariff.id}.toJson),
      "startFeeSubTotal" -> JsNumber(sessionNFees.startFeeSubTotal),
      "hourlyFeeSubTotal" -> JsNumber(sessionNFees.hourlyFeeSubTotal),
      "feePerKWhSubTotal" -> JsNumber(sessionNFees.feePerKWhSubTotal),
      "total" -> JsNumber(sessionNFees.total)
    )
  }
}


object SessionDBUtils {
  import SessionValidation._

  def validateNsaveSession(str: String, sessionRepository: SessionRepository)
                          (implicit ec: ExecutionContext): Future[String] = {
    val possibleSession = validateSessionFormat(str)

    possibleSession match {
      case Left(listOfErrors) =>
        Future.successful {
          listOfErrors.map(err => err.message).mkString("\n")
        }

      case Right(session) =>
        sessionRepository.create(session).map(_ => SuccessFullSessionInsertion.message)

    } //end of match
  } //end of def


  import SessionJsonProtocol._

  def all(sessionRepository: SessionRepository, outputType: String)(implicit ec: ExecutionContext): Future[String] = {
    outputType.toLowerCase() match {
      case "json" =>
        import spray.json._
        val futureAll = sessionRepository.all
        futureAll.map { lst =>
          lst.map { session =>
            session.toJson
          }.toJson.toString()
        } //future map

      case "csv" =>
        import purecsv.safe._
        val futureAll = sessionRepository.all
        futureAll.map { lst =>
          lst.map { sess =>
            (sess.id, sess.customerId, dateFormatter.format(sess.startTime),
              dateFormatter.format(sess.endTime), sess.volume)
          }.toCSV(",")
        } //future map

      case _ => Future.successful {
        UnknownOutputType.message
      }
    } // end of match
  } //end of def

  def listByCustomer(sessionRepository: SessionRepository, customerId: String, outputType: String)
                    (implicit ec: ExecutionContext): Future[String] = {
    outputType.toLowerCase() match {
      case "json" =>
        import spray.json._
        val futureList = sessionRepository.findByCustomerId(customerId)
        futureList.map { lst =>
          lst.map { session =>
            session.toJson
          }.toJson.toString()
        } //future map

      case "csv" =>
        import purecsv.safe._
        val futureAll = sessionRepository.findByCustomerId(customerId)
        futureAll.map { lst =>
          lst.map { sess =>
            (sess.id, sess.customerId, dateFormatter.format(sess.startTime),
              dateFormatter.format(sess.endTime), sess.volume)
          }.toCSV(",")
        } //future map

      case _ => Future.successful {
        UnknownOutputType.message
      }
    } // end of match
  }

  implicit def ordered: Ordering[Timestamp] = new Ordering[Timestamp] {
    def compare(x: Timestamp, y: Timestamp): Int = x compareTo y
  }

  def zipSessionNTarrif(sess: Session, tariffs: Seq[Tariff]): SessionNFees = {

    val resultTariffBeforeStartDate = tariffs
      .filter { tariff => tariff.activeStarting.compareTo(sess.startTime) < 1 }
      .sortBy(_.activeStarting).reverse

    val startTariff = resultTariffBeforeStartDate.head
    val tariffRange = tariffs
      .filter { tariff =>
        (tariff.activeStarting.compareTo(startTariff.activeStarting) >= 0) &&
          (tariff.activeStarting.compareTo(sess.endTime) < 0)
      }.sortBy(_.activeStarting)

    val totalStartFee = tariffRange.head.startFee.getOrElse(0.0)
    val totalFeePerKWh = tariffRange.head.feePerKWh.getOrElse(0.0) * sess.volume

    val hourlyRangesBuffer = mutable.Buffer[(Timestamp, Tariff)]()
    hourlyRangesBuffer += Tuple2(sess.startTime, tariffRange.head)
    hourlyRangesBuffer += Tuple2(sess.endTime, tariffRange.last)
    hourlyRangesBuffer ++= tariffRange.map(tariff => Tuple2(tariff.activeStarting, tariff))
    val hourlyRange = hourlyRangesBuffer.toList.sortBy(_._1)
    val intervals = hourlyRange.sliding(2).toList.
      map { case Seq(a, b) => millisToHours(diffTimeStamp(b._1, a._1)) * a._2.hourlyFee.getOrElse(0.0) }

    val totalhourlyFee = intervals.sum
    SessionNFees(sess.customerId, sess.startTime
      , sess.endTime, sess.volume
      , sess.id, tariffRange, totalStartFee, totalhourlyFee, totalFeePerKWh
      , totalStartFee + totalhourlyFee + totalFeePerKWh
    )
  }

  def totalfeeByCustomer(sessionRepository: SessionRepository, customerId: String, outputType: String)
                        (implicit ec: ExecutionContext): Future[String] = {
    outputType.toLowerCase() match {
      case "json" =>
        import spray.json._
        import SessionNFeesJsonProtocol._

        val futureList = sessionRepository.feesByCustomerId(customerId)
        futureList.map { case (sessionLst, tariffs) =>
          sessionLst.map { sess =>
            val sessionNFees: SessionNFees = zipSessionNTarrif(sess, tariffs)
            sessionNFees.toJson
          }.toJson.toString()
        } //future map

      case "csv" =>
        import purecsv.safe._
        val future = sessionRepository.feesByCustomerId(customerId)

        future.map { case (sessionLst, tariffs) =>
          sessionLst.map { sess =>
            val sessionNFees: SessionNFees = zipSessionNTarrif(sess, tariffs)
            (sessionNFees.customerId,
              dateFormatter.format(sessionNFees.startTime),
              dateFormatter.format(sessionNFees.endTime),
              sessionNFees.volume,
              sessionNFees.startFeeSubTotal,
              sessionNFees.hourlyFeeSubTotal,
              sessionNFees.feePerKWhSubTotal,
              sessionNFees.total
            )
          }.toCSV(",")
        } //future map

      case _ => Future.successful {
        UnknownOutputType.message
      }
    }
  } // end of totalfeeByCustomer
}