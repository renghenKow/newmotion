package com.renghen.models

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.{Locale, TimeZone}

import com.renghen.models.utils.Messages.{SuccessFullTariffInsertion, TooEarly}
import com.renghen.repository.TariffRepository
import slick.jdbc.MySQLProfile.api._
import com.renghen.models.utils.SetTariffValidation
import spray.json.{DefaultJsonProtocol, JsNumber, JsObject, JsString, JsonWriter}

import scala.concurrent.{ExecutionContext, Future}

case class Tariff(currency: String, activeStarting: Timestamp,
                  startFee: Option[Double], hourlyFee: Option[Double],
                  feePerKWh: Option[Double], id: Option[Long] = None)


object TariffJsonProtocol extends DefaultJsonProtocol {

  implicit object TariffJsonFormat extends JsonWriter[Tariff] {
    val sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault) //spec for RFC3339 with a 'Z'
    sdf.setTimeZone(TimeZone.getTimeZone("UTC"))

    def write(tariff: Tariff) = JsObject(
      "currency" -> JsString(tariff.currency),
      "activeStarting" -> JsString(sdf.format(tariff.activeStarting)),
      "startFee" -> JsNumber(tariff.startFee.getOrElse(0.0)),
      "hourlyFee" -> JsNumber(tariff.hourlyFee.getOrElse(0.0)),
      "feePerKWh" -> JsNumber(tariff.feePerKWh.getOrElse(0.0))
    )
  }

}

trait TariffTable {

  class Tariffs(tag: Tag) extends Table[Tariff](tag, "tariff") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc) // primary key,just a dummy one

    def currency = column[String]("currency")

    def activeStarting = column[Timestamp]("activeStarting")

    def startFee = column[Option[Double]]("startFee")

    def hourlyFee = column[Option[Double]]("hourlyFee")

    def feePerKWh = column[Option[Double]]("feePerKWh")

    def * = (currency, activeStarting, startFee, hourlyFee, feePerKWh, id) <> ((Tariff.apply _).tupled, Tariff.unapply)
  }

  protected val tariffs = TableQuery[Tariffs]

}

object TariffUtils {
  import SetTariffValidation._

  def validateNsaveTariff(str: String, tariffRepository: TariffRepository)
                         (implicit ec: ExecutionContext): Future[String] = {
    val possibleTariff = validateSetTariffFormat(str)

    possibleTariff match {
      case Left(listOfErrors) =>
        Future.successful {
          listOfErrors.map(err => err.message).mkString("\n")
        }

      case Right(tariff) =>
        val tariffValidDate = isTariffLatest(tariff, tariffRepository)
        tariffValidDate.flatMap { validatedTariff =>
          if (validatedTariff) {
            tariffRepository.create(tariff).map(_ => SuccessFullTariffInsertion.message)
          } else Future.successful(TooEarly.message)
        } //end of flatmap
    } //end of match
  } //end of def

  import TariffJsonProtocol._

  def all(tariffRepository: TariffRepository)(implicit ec: ExecutionContext): Future[String] = {
    import spray.json._
    val futureAll = tariffRepository.all
    futureAll.map { all =>
      all.map { tariff =>
        tariff.toJson
      }.toJson.toString()
    } //future map
  } //end of def

}



