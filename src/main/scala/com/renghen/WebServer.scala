package com.renghen

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import repository.{SessionRepository, TariffRepository}
import services._

import scala.concurrent.ExecutionContext
import scala.io.StdIn

object WebServer extends App
  with ConfigService
  with WebApi {

  // WebApi
  override implicit val system: ActorSystem = ActorSystem("Akka_HTTP_NewMotion")
  override implicit val executor: ExecutionContext = system.dispatcher
  override implicit val materializer: ActorMaterializer = ActorMaterializer()

  val databaseService: DatabaseService = new MysqlService(dataConfigName)
  val tariffRepository = new TariffRepository(databaseService)
  val sessionRepository = new SessionRepository(databaseService)
  val apiService = new ApiService(tariffRepository,sessionRepository)

  val bindingFuture = Http().bindAndHandle(apiService.routes, httpHost, httpPort)

  println(s"Server online at $httpHost:$httpPort/\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  databaseService.db.close()
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done

}
