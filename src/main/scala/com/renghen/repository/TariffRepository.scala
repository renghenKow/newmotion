package com.renghen.repository

import com.renghen.models._
import com.renghen.services.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

class TariffRepository(val databaseService: DatabaseService)(implicit executor: ExecutionContext) extends TariffTable {
  import databaseService._
  import databaseService.driver.api._

  // add a new Tariff
  def create(tariff: Tariff): Future[Int] = db.run(tariffs += tariff)

  def all: Future[Seq[Tariff]] = db.run(tariffs.result)

  def delete(id: Long): Future[Int] = db.run(tariffs.filter(_.id === id).delete)

  def clean(): Future[Int] = db.run(tariffs.delete)

  def findLatest(): Future[Option[Tariff]] = db.run(tariffs.sortBy(_.activeStarting.desc).take(1).result.headOption)

}
