package com.renghen.repository

import java.sql.Timestamp

import com.renghen.models
import com.renghen.models.{SessionTable, Tariff, TariffTable}
import com.renghen.services.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

class SessionRepository(val databaseService: DatabaseService)
                       (implicit executor: ExecutionContext)
  extends SessionTable with TariffTable {

  import databaseService._
  import databaseService.driver.api._

  // add a new Tariff
  def create(session: models.Session): Future[Int] = db.run(sessions += session)

  def all: Future[Seq[models.Session]] = db.run(sessions.result)

  def delete(id: Long): Future[Int] = db.run(sessions.filter(_.id === id).delete)

  def clean(): Future[Int] = db.run(sessions.delete)

  def findByCustomerId(customerId: String): Future[Seq[models.Session]] = {
    val query = sessions.filter { sessions => sessions.customerId === customerId }.sortBy(_.startTime)
    db.run(query.result)
  }


  def feesByCustomerId(customerId: String): Future[(Seq[models.Session],Seq[Tariff])] = {
    val query = sessions.filter { sessions => sessions.customerId === customerId }.sortBy(_.startTime)
    val sessionOfCustomer = db.run(query.result)
    val tariffList: Future[Seq[Tariff]] = sessionOfCustomer.flatMap { sessionLst =>
      val tariffRange = if(sessionLst.isEmpty){
        Future.successful(Nil)
      }
      else{
          val sess = sessionLst.head
          val queryTariffBeforeStartDate = tariffs
            .filter { tariff => tariff.activeStarting < sess.startTime }
            .sortBy(_.activeStarting.desc).result

          val resultTariffBeforeStartDate = db.run(queryTariffBeforeStartDate)
          resultTariffBeforeStartDate.flatMap { tariff =>
            tariff.headOption match {
              case None => Future.successful(Nil)
              case Some(startTariff) =>
                val queryTariffRange = tariffs.filter { tariff =>
                  (tariff.activeStarting >= startTariff.activeStarting) &&
                    (tariff.activeStarting < sessionLst.last.endTime)
                } //filter
                db.run(queryTariffRange.result)
            } // end of match
          } //end of flatmap
      }//end of else
      tariffRange
    }
    sessionOfCustomer.zip(tariffList)
  }

  //def findLatest(): Future[Option[Tariff]] = db.run(sessions.sortBy(_.activeStarting.desc).take(1).result.headOption)

}
