package com.renghen.services

import com.typesafe.config.ConfigFactory


trait ConfigService {

  // Load the default config file
  private val config = ConfigFactory.load()
  // Load the "http" config object
  private val httpConfig = config.getConfig("http")
  // Load the "database" config object
  private val databaseConfig = config.getConfig("mysql")

  val dataConfigName: String = "mysql"
  val httpHost: String = httpConfig.getString("interface")
  val httpPort: Int = httpConfig.getInt("port")

  val jdbcUrl: String = databaseConfig.getString("properties.url")
  val dbUser: String = databaseConfig.getString("properties.user")
  val dbPassword: String = databaseConfig.getString("properties.password")
  val profile: String = databaseConfig.getString("profile")

}
