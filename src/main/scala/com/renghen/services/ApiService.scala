package com.renghen.services

//akka libs
import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer

//custom libs
import com.renghen.controllers.{SessionController, TariffController}
import com.renghen.repository.{SessionRepository, TariffRepository}

//scala libs
import scala.concurrent.ExecutionContext

class ApiService(
                  tariffRepository : TariffRepository,
                  sessionRepository : SessionRepository
                )(implicit executor: ExecutionContext, as: ActorSystem, mat: Materializer) {

  val tariffController = new TariffController(tariffRepository)
  val sessionController = new SessionController(sessionRepository)

  def routes: Route =
    pathPrefix("api") {
      tariffController.routes ~
      sessionController.routes
    }
}