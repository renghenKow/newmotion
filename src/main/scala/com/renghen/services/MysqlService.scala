package com.renghen.services

import com.typesafe.config.Config
import slick.jdbc.{JdbcProfile, MySQLProfile}
import slick.jdbc.MySQLProfile.api._

class MysqlService(config : String) extends DatabaseService {

  // Setup our database driver, Postgres in this case
  val driver: JdbcProfile = MySQLProfile

  override val db = Database.forConfig(config)
  db.createSession()
}