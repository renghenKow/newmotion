package com.renghen.controllers

/*
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.unmarshalling.{PredefinedFromStringUnmarshallers, Unmarshaller}

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.IntNumber
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.Done
*/

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.util.Timeout
import com.renghen.models.{SessionDBUtils, TariffUtils}
import com.renghen.repository.SessionRepository

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}


class SessionController(val sessionRepository: SessionRepository)(implicit val ec: ExecutionContext) {
  implicit val timeout: Timeout = 5.seconds

  val routes: Route =
    pathPrefix("session") {
      post {
        path("add") {
          entity(as[String]) { sessionStr: String =>
            val saved = SessionDBUtils.validateNsaveSession(sessionStr, sessionRepository)

            onComplete(saved) { done =>
              done match {
                case Success(res) => complete(res)
                case Failure(_) => complete("something wrong")
              }
            } //oncomplete
          }
        } //path setTariff
      } ~ //Post /add
        get {
          path("all") {
            parameters('type.?) { outputType =>
              val typeStr = outputType.getOrElse("json")
              val output = SessionDBUtils.all(sessionRepository, typeStr)
              onComplete(output) { done =>
                done match {
                  case Success(res) => complete(res)
                  case Failure(_) => complete("something wrong")
                }
              } //oncomplete
            }
          } // path all
        } ~ //Get /all
        get {
          path("list" / Segment) { customerId =>
            parameters('type.?) { outputType =>
              val typeStr = outputType.getOrElse("json")
              val output = SessionDBUtils.listByCustomer(sessionRepository, customerId,typeStr)

              onComplete(output) { done =>
                done match {
                  case Success(res) => complete(res)
                  case Failure(_) => complete("something wrong")
                }
              } // oncomplete
            }
          } // path list
        } ~ //Get /all
        get {
          path("listTotal" / Segment) { customerId =>
            parameters('type.?) { outputType =>
              val typeStr = outputType.getOrElse("json")
              val output = SessionDBUtils.totalfeeByCustomer(sessionRepository, customerId,typeStr)

              onComplete(output) { done =>
                done match {
                  case Success(res) => complete(res)
                  case Failure(_) => complete("something wrong")
                }
              } // oncomplete
            }
          } // path list
        }

    }

  /*
    val routes = pathPrefix("books") {
      pathEndOrSingleSlash {
        post {
          // From it's documentation: Decompresses the incoming request if it is `gzip` or `deflate` compressed. Uncompressed requests are passed through untouched.
          decodeRequest {
            // Parses the request as the given entity, in this case a `Book`
            entity(as[Book]) { book =>
              println(s"BOOK: $book")
              complete(StatusCodes.Created, bookRepository.create(book))
            }
          }
        } ~
          get {
            parameters(('title.?, 'releaseDate.as[Date].?, 'categoryId.as[Long].?, 'author.?))
              .as(BookSearch) { bookSearch: BookSearch =>
                complete {
                  bookRepository.search(bookSearch)
                }
              }
          }
      } ~
        // After the `/books/` we expect the id of the book we want to delete
        pathPrefix(IntNumber) { id =>
          // We want to listen to paths like `/books/id` or `/books/id/`
          pathEndOrSingleSlash {
            get {
              verifyToken { user =>
                complete {
                  bookRepository.findById(id)
                }
              }
            } ~
              delete {
                onSuccess(bookRepository.delete(id)) {
                  // If we get a number higher than 0, it deleted the book
                  case n if n > 0 => complete(StatusCodes.NoContent)
                  // If 0 is returned, the book wasn't found
                  case _ => complete(StatusCodes.NotFound)
                }
              }
          }
        }
    }*/

}