CREATE TABLE newMotion.tariff (
 id INT NOT NULL AUTO_INCREMENT ,
 currency VARCHAR(3) NOT NULL ,
 startFee DOUBLE NULL ,
 hourlyFee DOUBLE NULL ,
 feePerKWh DOUBLE NULL ,
 activeStarting DATETIME NOT NULL,
 PRIMARY KEY (id)) ENGINE = InnoDB;