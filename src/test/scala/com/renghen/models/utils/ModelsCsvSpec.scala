package com.renghen.models.utils

import org.scalatest.FlatSpec

class ModelsCsvSpec extends FlatSpec {
  import LoadCurrencyCode._

  "number of codes" should "be 299" in {
    assert(currencyCodes.length === 299)
  }

  "codes" should "contain EUR" in {
    assert(currencyCodes.find(code => code == "EUR") !== None)
  }

  "codes" should "not have duplicates" in {
    val duplicates = currencyCodes.groupBy(identity)
      .collect { case (x,ys) if ys.lengthCompare(1) > 0 => x }
    assert(duplicates.isEmpty)
  }

}
