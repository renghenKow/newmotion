package com.renghen.models.utils

import org.scalatest.FlatSpec
import spray.json.JsonParser

class ModelsJsonTask1 extends FlatSpec {

  import ValidateFunctions._

  private val invalidJson1 = "I am not a json"
  invalidJson1 should "not parse as a json" in {
    val isJsonParsed = isJson(invalidJson1)
    assert(isJsonParsed.isLeft)
  }

  private val invalidJson2 = """{a:4}"""
  invalidJson2 should "not parse as a json" in {
    val isJsonParsed = isJson(invalidJson2)
    assert(isJsonParsed.isLeft)
  }

  private val invalidJson3 = """{'a':4}"""
  invalidJson3 should "not parse as a json" in {
    val isJsonParsed = isJson(invalidJson3)
    assert(isJsonParsed.isLeft)
  }

  private val validJson = """{"a":4}"""
  validJson should "parse as a json" in {
    val isJsonParsed = isJson(validJson)
    assert(isJsonParsed.isRight)
  }

  private val validCurrency = """{"currency": "EUR"}"""
  validCurrency should "have a valid currency" in {
    val jsValue = JsonParser(validCurrency)
    val isCurrencyValid = isValidCurrency("currency", jsValue)
    assert(isCurrencyValid.isRight)
  }

  private val invalidCurrency = """{"currency": "EU"}"""
  invalidCurrency should "have a invalid currency" in {
    val jsValue = JsonParser(invalidCurrency)
    val isCurrencyValid = isValidCurrency("currency", jsValue)
    assert(isCurrencyValid.isLeft)
  }

  private val validDate = """{"activeStarting" : "2017-01-01T00:00:00.000Z"}"""
  validDate should "have a valid date" in {
    val jsValue = JsonParser(validDate)
    val isDateValid = isFieldValidDate("activeStarting", jsValue)
    assert(isDateValid.isRight)
  }

  private val invalidDate = """{"activeStarting" : "2017-01-01"}"""
  invalidDate should "have a invalid date" in {
    val jsValue = JsonParser(invalidDate)
    val isDateValid = isFieldValidDate("activeStarting", jsValue)
    assert(isDateValid.isLeft)
  }

  /**
    * "startFee": 0.20,
    * "hourlyFee": 1.00,
    * "feePerKWh": 0.25,
    * "activeStarting": "2017-01-0
    */
  private val noFee =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR"}""".stripMargin
  noFee should "have a no fee" in {
    val jsValue = JsonParser(noFee)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil

    assert(results.count(value => value.isLeft) === 3)
  }


  private val startFeeOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"startFee": 0.20}""".stripMargin
  startFeeOnly should "have 1 fee only" in {
    val jsValue = JsonParser(startFeeOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 1)
  }

  private val invalidStartFee =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"startFee": "qw"}""".stripMargin
  invalidStartFee should "have invalid startFee" in {
    val jsValue = JsonParser(invalidStartFee)
    val results = for{
      startFee <-isFieldPresent("startFee", jsValue)
      doubleValue <- isValidCurrency("startFee",jsValue)
    } yield doubleValue

    assert(results.isLeft)
  }

  private val invalidStartFee2 =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR"}""".stripMargin
  invalidStartFee2 should "have invalid startFee" in {
    val jsValue = JsonParser(invalidStartFee2)
    val results = for{
      startFee <-isFieldPresent("startFee", jsValue)
      doubleValue <- isValidCurrency("startFee",jsValue)
    } yield doubleValue

    assert(results.isLeft)
  }

  private val hourlyFeeOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"hourlyFee": 1.00}""".stripMargin
  hourlyFeeOnly should "have 1 fee only" in {
    val jsValue = JsonParser(hourlyFeeOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 1)
  }

  private val invalidHourlyFee =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"hourlyFee": "qw"}""".stripMargin
  invalidHourlyFee should "have invalid hourlyFee" in {
    val jsValue = JsonParser(invalidHourlyFee)
    val results = for{
      startFee <-isFieldPresent("hourlyFee", jsValue)
      doubleValue <- isValidCurrency("hourlyFee",jsValue)
    } yield doubleValue

    assert(results.isLeft)
  }

  private val invalidHourlyFee2 =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR"}""".stripMargin
  invalidHourlyFee2 should "have invalid hourlyFee" in {
    val jsValue = JsonParser(invalidHourlyFee2)
    val results = for{
      startFee <-isFieldPresent("hourlyFee", jsValue)
      doubleValue <- isValidCurrency("hourlyFee",jsValue)
    } yield doubleValue

    assert(results.isLeft)
  }

  private val feePerKWhOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"feePerKWh": 0.25}""".stripMargin
  feePerKWhOnly should "have 1 fee only" in {
    val jsValue = JsonParser(feePerKWhOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 1)
  }
  private val invalidFeePerKWh =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"feePerKWh": "wq"}""".stripMargin
  invalidFeePerKWh should "have  invalid" in {
    val jsValue = JsonParser(invalidFeePerKWh)
    val results = for{
      startFee <-isFieldPresent("feePerKWh", jsValue)
      doubleValue <- isValidCurrency("feePerKWh",jsValue)
    } yield doubleValue
    assert(results.isLeft)
  }

  private val invalidFeePerKWh2 =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR"}""".stripMargin
  invalidFeePerKWh2 should "have  invalid" in {
    val jsValue = JsonParser(invalidFeePerKWh2)
    val results = for{
      startFee <-isFieldPresent("feePerKWh", jsValue)
      doubleValue <- isValidCurrency("feePerKWh",jsValue)
    } yield doubleValue
    assert(results.isLeft)
  }

  private val startFeeNhourlyFeeOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"startFee": 0.20,
      |"hourlyFee": 1.00}""".stripMargin
  startFeeNhourlyFeeOnly should "have 2 fee only" in {
    val jsValue = JsonParser(startFeeNhourlyFeeOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 2)
  }

  private val startFeeNfeePerKWhOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"startFee": 0.20,
      |"feePerKWh": 0.25}""".stripMargin
  startFeeNfeePerKWhOnly should "have 2 fee only" in {
    val jsValue = JsonParser(startFeeNfeePerKWhOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 2)
  }

  private val hourlyFeeNfeePerKWhOnly =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"hourlyFee": 1.00,
      |"feePerKWh": 0.25}""".stripMargin
  hourlyFeeNfeePerKWhOnly should "have 2 fee only" in {
    val jsValue = JsonParser(hourlyFeeNfeePerKWhOnly)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 2)
  }

  private val allFees =
    """{"activeStarting" : "2017-01-01T00:00:00.000Z",
      |"currency" : "EUR",
      |"startFee": 0.20,
      |"hourlyFee": 1.00,
      |"feePerKWh": 0.25}""".stripMargin
  allFees should "have 3 fee only" in {
    val jsValue = JsonParser(allFees)
    val results = isFieldPresent("startFee", jsValue) ::
      isFieldPresent("hourlyFee", jsValue) ::
      isFieldPresent("feePerKWh", jsValue) :: Nil
    assert(results.count(value => value.isRight) === 3)
  }



}
