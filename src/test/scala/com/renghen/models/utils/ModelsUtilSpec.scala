package com.renghen.models.utils

import org.scalatest.FlatSpec
import com.renghen.models.utils
import cats._
import com.renghen.utils.DateUtil

import scala.util.Try

class ModelsUtilSpec extends FlatSpec{

  private val dateRfcWithZ = "2017-04-12T23:20:50.52Z"

  dateRfcWithZ should "be a correct RFC 3339 date" in {
    val date = DateUtil.parseRFC3339Date(dateRfcWithZ)
    assert(date.getTime == 1492039250052l)
  }

  private val dateRfcWithoutZ = "2018-01-19T16:39:57-08:00"
  dateRfcWithoutZ should "be a correct RFC 3339 date" in {
    val date = DateUtil.parseRFC3339Date(dateRfcWithoutZ)
    assert(date.getTime == 1516408797000l)
  }

  private val dateFailure = "2018-01-19T16:39:57"
  dateFailure should "fail parsing" in {
    val dateTry = Try(DateUtil.parseRFC3339Date(dateFailure))
    assert(dateTry.isFailure)
  }

}
