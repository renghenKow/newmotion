lazy val akkaHttpVersion = "10.1.1"
lazy val akkaVersion    = "2.5.11"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.newMotion",
      scalaVersion    := "2.12.5"
    )),
    name := "akka-http",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http"            % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml"        % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream"          % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j"           % akkaVersion,

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.1"         % Test,

      //db libs
      "com.typesafe.slick" %% "slick" % "3.2.2",
      "mysql" % "mysql-connector-java" % "6.0.6",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.2.2",
      "org.slf4j" % "slf4j-nop" % "1.7.25" % Test,
      "org.flywaydb" % "flyway-core" % "5.0.7",

      //misc lib
      "org.typelevel" %% "cats-core" % "1.1.0",
      "io.spray" %%  "spray-json" % "1.3.4",
      "com.github.melrief" %% "purecsv" % "0.1.1"
    ),
    scalacOptions += "-Ypartial-unification"
  )
